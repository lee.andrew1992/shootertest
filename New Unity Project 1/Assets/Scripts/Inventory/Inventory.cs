﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlaneGame.ConstantVariables;

public class Inventory : MonoBehaviour {

    public Weapons[] weaponSlots = new Weapons [ConstantValues.Weapons.WeaponSlots.MaxSlots]; 
    public int currWeaponIndex;

	// Use this for initialization
	void Start () {
        currWeaponIndex = 0;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
