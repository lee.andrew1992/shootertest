﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlaneGame.ConstantVariables;

public class SF_Pistol : Weapons {

	// Use this for initialization
	void Start () {
        Init(
            WeaponType.SingleFire,
            ConstantValues.Weapons.WeaponValues.SingleFire.Pistol.Moniker,
            ConstantValues.Weapons.WeaponValues.SingleFire.Pistol.Damage,
            ConstantValues.Weapons.WeaponValues.SingleFire.Pistol.FireRate,
            ConstantValues.Weapons.WeaponValues.SingleFire.Pistol.ClipSize,
            ConstantValues.Weapons.WeaponValues.SingleFire.Pistol.ReloadTime,
            ConstantValues.Weapons.WeaponValues.SingleFire.Pistol.BulletSpeed,
            ConstantValues.Weapons.WeaponValues.SingleFire.Pistol.Recoil,
            ConstantValues.Weapons.WeaponValues.SingleFire.Pistol.MaxRecoil,
            ConstantValues.Weapons.WeaponValues.SingleFire.Pistol.RecoilResetTime
        );
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonUp(ConstantValues.InputVariables.ControlKeys.Fire))
        {
            singleFireReady = true;
        }
    }
}
