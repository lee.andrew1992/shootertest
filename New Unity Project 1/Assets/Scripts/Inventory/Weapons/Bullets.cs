﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlaneGame.ConstantVariables;
using PlaneGame.GameState;

public class Bullets : MonoBehaviour {

    private bool active = false;
    private float damage;
    private float lifeTime = -1f;
    private Vector3 target;
    private float speed;

    private Vector3 movementVector = Vector3.zero;
	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
        if (active)
        {
            if (lifeTime <= 0)
            {
                DestroyBullet();
            }
            else
            {
                transform.position += movementVector * Time.deltaTime;
            }
            lifeTime -= Time.deltaTime;
        }
	}

    public void InitBullet(float bulletDamage, float bulletSpeed, Vector3 bulletTarget) {
        active = true;
        damage = bulletDamage;
        lifeTime = ConstantValues.Weapons.WeaponValues.BulletLifeTime;
        speed = bulletSpeed;
        target = bulletTarget;
        movementVector = (target - transform.position).normalized * speed;
        GameState.Player.bulletList.Add(this);
    }

    public void DestroyBullet()
    {
        Destroy(this.gameObject);
        GameState.Player.bulletList.Remove(this);
    }

    void OnCollisionEnter()
    {
        DestroyBullet();
    } 
}
