﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlaneGame.ConstantVariables;
using PlaneGame.GameState;

public class Weapons : MonoBehaviour {

    protected enum WeaponType
    {
        SingleFire,
        AutoFire
    };

    protected string moniker;
    protected int clipSize;
    protected int currAmmoCount;
    protected WeaponType type;
    protected float damage;
    protected float fireRate;
    protected float fireRateEffect;
    protected float reloadTime;
    protected float bulletSpeed;
    protected bool singleFireReady = true;
    protected float recoil;
    protected float maxRecoil;
    protected float recoilResetTime;

    [SerializeField]
    protected GameObject bullet;
    protected bool reloading = false;

    protected float currRecoil = 0;
    protected float lastShotTime = 0;

    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {
        foreach (Weapons weapon in GameState.Player.myInventory.weaponSlots)
        {
            if (weapon)
            {
                weapon.CalculateFireRate();
                weapon.CalculateRecoil();
            }
        }
    }

    public virtual bool Fire(Vector3 target) {
        if (currAmmoCount > 0 && !reloading && fireRateEffect <= 0 && singleFireReady)
        {
            fireRateEffect = fireRate;

            if (type == WeaponType.SingleFire)
            {
                singleFireReady = false;
            }

            float aimPointDist = Vector3.Distance(GameState.Player.unit.transform.position, GameState.Player.aimPoint.transform.position);
            float recoilValue = (((recoil + 1) * (recoil + 1)) - 1) * Mathf.Min(aimPointDist / ConstantValues.Weapons.WeaponValues.RecoilNormalize, 1);
            currRecoil = Mathf.Min(currRecoil + recoilValue, maxRecoil);
            ShootBullet(target);
            return true;
        }
        else
        {
            return false;
        }
    }

    public virtual void Reload()
    {
        if (currAmmoCount < clipSize)
        {
            StartCoroutine(ActualReload());
        }
        else
        {
            // No Need to reload
        }
    }

    public virtual void ShootBullet(Vector3 target)
    {
        currAmmoCount--;
        GameObject thisBullet = Instantiate(bullet, GameState.Player.unit.transform.position, Quaternion.Euler(Vector3.zero));

        float negPosModifier = Random.Range(-1.0f, 1.0f);
        float recoilEffect = (Random.Range((currRecoil * 0.5f), currRecoil) * negPosModifier);

        switch (GameState.currView)
        {
            case GameState.CameraView.TopDown:
                target = new Vector3(target.x + recoilEffect, target.y, target.z);
                break;

            case GameState.CameraView.SideView:
                target = new Vector3(target.x, target.y + recoilEffect, target.z);
                break;

            case GameState.CameraView.Action:
                target = new Vector3(target.x + recoilEffect, target.y + recoilEffect, target.z);
                break;
        }

        thisBullet.GetComponent<Bullets>().InitBullet(damage, bulletSpeed, target);
        lastShotTime = Time.time;
    }

    protected virtual void Init(WeaponType weaponType, string weaponName, float weaponDamage, float weaponFireRate, int weaponClipSize, float weaponReloadTime, float weaponBulletSpeed, float weaponRecoil, float weaponMaxRecoil, float weaponRecoilResetTime)
    {
        type = weaponType;
        moniker = weaponName;
        damage = weaponDamage;
        fireRate = weaponFireRate;
        clipSize = weaponClipSize;
        reloadTime = weaponReloadTime;
        bulletSpeed = weaponBulletSpeed;
        currAmmoCount = clipSize;
        recoil = weaponRecoil;
        maxRecoil = weaponMaxRecoil;
        recoilResetTime = weaponRecoilResetTime;
        singleFireReady = true; 
    }

    private IEnumerator ActualReload()
    {
        reloading = true;
        yield return new WaitForSeconds(reloadTime);
        currAmmoCount = clipSize;
        reloading = false;
        currRecoil = 0;
    }

    protected void CalculateFireRate() {

        if (fireRateEffect > 0)
        {
            fireRateEffect -= Time.deltaTime;
        }
    }

    protected void CalculateRecoil()
    {
        if (currAmmoCount > 0)
        {
            float timeDiff = Time.time - lastShotTime;
            if (timeDiff < recoilResetTime)
            {
                currRecoil = Mathf.Max(currRecoil - (currRecoil * (timeDiff / recoilResetTime)), 0);
            }
            else
            {
                currRecoil = 0;
            }
        }
    }
}