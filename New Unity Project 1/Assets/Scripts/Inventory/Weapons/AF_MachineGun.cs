﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlaneGame.ConstantVariables;

public class AF_MachineGun : Weapons {

	// Use this for initialization
	void Start () {
        Init(
            WeaponType.AutoFire,
            ConstantValues.Weapons.WeaponValues.AutoFire.MachineGun.Moniker,
            ConstantValues.Weapons.WeaponValues.AutoFire.MachineGun.Damage,
            ConstantValues.Weapons.WeaponValues.AutoFire.MachineGun.FireRate,
            ConstantValues.Weapons.WeaponValues.AutoFire.MachineGun.ClipSize,
            ConstantValues.Weapons.WeaponValues.AutoFire.MachineGun.ReloadTime,
            ConstantValues.Weapons.WeaponValues.AutoFire.MachineGun.BulletSpeed,
            ConstantValues.Weapons.WeaponValues.AutoFire.MachineGun.Recoil,
            ConstantValues.Weapons.WeaponValues.AutoFire.MachineGun.MaxRecoil,
            ConstantValues.Weapons.WeaponValues.AutoFire.MachineGun.RecoilResetTime
        );
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
