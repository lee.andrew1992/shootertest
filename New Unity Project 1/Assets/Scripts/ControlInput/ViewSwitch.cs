﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlaneGame.ConstantVariables;
using PlaneGame.GameState;
using PlaneGame.UtilityFunctions;
using System;

public class ViewSwitch : MonoBehaviour {

    public float initialSpeed;
    public float minimumSpeed;
    public float speedReductionRate;
    public float waitBetweenViews;

    public bool swapping = false;

    //GameState.CameraView currView;
    void Awake()
    {
        GameState.currView = GameState.CameraView.TopDown;
        GameState.Player.viewSwitch = this;
    }

    void Start () {

    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetButtonDown(ConstantValues.InputVariables.ControlKeys.ViewSwap))
        {
            swapping = true;
            Queue<GameState.CameraView> destinationSteps = new Queue<GameState.CameraView>();

            if (GameState.currView == GameState.CameraView.TopDown)
            {
                destinationSteps.Enqueue(GameState.CameraView.Action);
                destinationSteps.Enqueue(GameState.CameraView.SideView);

                StartCoroutine(SwitchView(destinationSteps, initialSpeed, false));
            }
            else if (GameState.currView == GameState.CameraView.Action)
            {
            }
            else
            {
                destinationSteps.Enqueue(GameState.CameraView.Action);
                destinationSteps.Enqueue(GameState.CameraView.TopDown);

                StartCoroutine(SwitchView(destinationSteps, initialSpeed, false));
            }
        }
    }


    IEnumerator SwitchView(Queue<GameState.CameraView> targetViews, float startingSpeed, bool resetPos = true, bool slowDown = true)
    {
        GameState.CameraView targetView = targetViews.Peek();
        GameState.currView = targetView;

        GameState.SetHitZone(targetView);

        while (swapping)
        {
            if (slowDown)
            {
                Time.timeScale = Mathf.Clamp(Time.timeScale - Time.deltaTime * speedReductionRate, ConstantValues.DefaultValues.Camera.ViewSwitch.SwapMinimumSpeed, ConstantValues.DefaultValues.Camera.ViewSwitch.SwapMaximumSpeed);
            }
            else
            {   
                Time.timeScale = Mathf.Clamp(Time.timeScale + Time.deltaTime * speedReductionRate, ConstantValues.DefaultValues.Camera.ViewSwitch.SwapMinimumSpeed, ConstantValues.DefaultValues.Camera.ViewSwitch.SwapMaximumSpeed);
            }

            Vector3 targetPosition = new Vector3();

            if (targetView == GameState.CameraView.TopDown)
            {
                targetPosition = ConstantValues.DefaultValues.Camera.ViewSwitch.TopDownView.Position;
                foreach (EnemyGroup group in GameState.createdEnemyGroups)
                {
                    group.SetEnemyViewPosition(targetView);
                }
            }
            else if (targetView == GameState.CameraView.SideView)
            {
                targetPosition = ConstantValues.DefaultValues.Camera.ViewSwitch.SideView.Position;
                foreach (EnemyGroup group in GameState.createdEnemyGroups)
                {
                    group.SetEnemyViewPosition(targetView);
                }
            }
            else
            {
                targetPosition = new Vector3 (ConstantValues.DefaultValues.Camera.ViewSwitch.ActionView.Position.x, ConstantValues.DefaultValues.Camera.ViewSwitch.ActionView.Position.y, GameState.Player.unit.transform.position.z + ConstantValues.DefaultValues.Camera.ViewSwitch.ActionView.Position.z );
                foreach (EnemyGroup group in GameState.createdEnemyGroups)
                {
                    group.SetEnemyViewPosition(targetView);
                }
            }

            if (resetPos)
            {
                GameState.Player.unit.GetComponent<Movement>().ResetPosition(targetView, startingSpeed);
            }

            GameState.Player.myCrosshair.SetCrossHairRotation(!swapping);

            float xAxisMod, yAxisMod, zAxisMod;

            xAxisMod = (transform.position.x > targetPosition.x) ? -1 : 1;
            yAxisMod = (transform.position.y > targetPosition.y) ? -1 : 1;
            zAxisMod = (transform.position.z > targetPosition.z) ? -1 : 1;

            transform.position = new Vector3(
                Mathf.Clamp(transform.position.x + (xAxisMod * startingSpeed * Time.deltaTime), Mathf.Min(targetPosition.x, transform.position.x), Mathf.Max(targetPosition.x, transform.position.x)),
                Mathf.Clamp(transform.position.y + (yAxisMod * startingSpeed * Time.deltaTime), Mathf.Min(targetPosition.y, transform.position.y), Mathf.Max(targetPosition.y, transform.position.y)),
                Mathf.Clamp(transform.position.z + (zAxisMod * startingSpeed * Time.deltaTime), Mathf.Min(targetPosition.z, transform.position.z), Mathf.Max(targetPosition.z, transform.position.z))
            );

            transform.LookAt(ConstantValues.DefaultValues.Camera.ScreenCenter.Position);

            if (Vector3.Distance(transform.position, targetPosition) == 0)
            {
                transform.position = targetPosition;
                swapping = false;
                GameState.Player.myCrosshair.SetCrossHairRotation(!swapping);
            }

            yield return null;
        }

        if (targetViews.Count > 1)
        {
            targetViews.Dequeue();
            swapping = true;
            yield return new WaitForSeconds(waitBetweenViews);
            yield return StartCoroutine(SwitchView(targetViews, Mathf.Max( initialSpeed * 1.5f, minimumSpeed), true, false));
        }
        else
        {
            Time.timeScale = ConstantValues.DefaultValues.Camera.ViewSwitch.SwapMaximumSpeed;
            yield return null;
        }
    }
}
