﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlaneGame.ConstantVariables;
using PlaneGame.GameState;

public class Shooting : MonoBehaviour {
    public Texture2D crossHair;
    private Inventory myInventory;
    LayerMask layerMask;
    RectTransform aim;

    private float cursorRadius;

    void Awake()
    {
    }

	// Use this for initialization
	void Start () {
        myInventory = GetComponent<Inventory>();
        // HitZone Layer
        //layerMask = LayerMask.GetMask(ConstantValues.DefaultValues.Layers.hitZone);
    }
	
	// Update is called once per frame
	void Update ()
    {        
        if (Input.GetButton(ConstantValues.InputVariables.ControlKeys.Fire))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit, Mathf.Infinity))
            {
                Vector3 target = Vector3.zero;
                Vector3 aimPointPos = GameState.Player.aimPoint.transform.position;

                switch (GameState.currView)
                {
                    case GameState.CameraView.TopDown:
                        target = new Vector3(aimPointPos.x, ConstantValues.DefaultValues.Camera.ViewSwitch.TopDownView.UnitRestrictionAxisY, aimPointPos.z);
                        break;

                    case GameState.CameraView.Action:
                        target = aimPointPos;
                        break;

                    case GameState.CameraView.SideView:
                        target = new Vector3(ConstantValues.DefaultValues.Camera.ViewSwitch.SideView.UnitRestrictionAxisX, aimPointPos.y, aimPointPos.z);
                        break;
                }
                myInventory.weaponSlots[myInventory.currWeaponIndex].Fire(target);
            }
        }

        if (Input.GetButtonDown(ConstantValues.InputVariables.ControlKeys.Reload))
        {
            myInventory.weaponSlots[myInventory.currWeaponIndex].Reload();
        }

        if (Input.GetButtonDown(ConstantValues.InputVariables.ControlKeys.WeaponSlot1))
        {
            if (GameState.Player.myInventory.currWeaponIndex != ConstantValues.Weapons.WeaponSlots.Slot1)
            {
                myInventory.currWeaponIndex = ConstantValues.Weapons.WeaponSlots.Slot1;
            }
        }
        if (Input.GetButtonDown(ConstantValues.InputVariables.ControlKeys.WeaponSlot2))
        {
            if (GameState.Player.myInventory.currWeaponIndex != ConstantValues.Weapons.WeaponSlots.Slot2)
            {
                myInventory.currWeaponIndex = ConstantValues.Weapons.WeaponSlots.Slot2;
            }
        }
        if (Input.GetButtonDown(ConstantValues.InputVariables.ControlKeys.WeaponSlot3))
        {
            if (GameState.Player.myInventory.currWeaponIndex != ConstantValues.Weapons.WeaponSlots.Slot3)
            {
                myInventory.currWeaponIndex = ConstantValues.Weapons.WeaponSlots.Slot3;
            }
        }
    }
}
