﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlaneGame.ConstantVariables;
using PlaneGame.GameState;
using PlaneGame.UtilityFunctions;
using System.Runtime.InteropServices;

public class Crosshair : MonoBehaviour {

    public GameObject cursorObject;
    GameObject aimPoint;
    bool resetting = false;
    public float aimPointSpeed;

    Vector2 limiter = new Vector2(1, 1);

    [DllImport("user32.dll")]
    static extern bool SetCursorPos(int X, int Y);

    float mouseMoveX;
    float mouseMoveY;

    // Use this for initialization
    void Start () {
        Init();
        aimPointSpeed = 0.5f;
    }
	
	// Update is called once per frame
	void Update ()
    {
        mouseMoveX = Input.GetAxis(ConstantValues.InputVariables.Axis.MouseX);
        mouseMoveY = Input.GetAxis(ConstantValues.InputVariables.Axis.MouseY);
        
        // Cursor Movement
        if (GameState.currView == GameState.CameraView.TopDown)
        {
            aimPoint.transform.position = (aimPoint.transform.position + new Vector3(mouseMoveX * aimPointSpeed, 0, mouseMoveY * aimPointSpeed));
            aimPoint.transform.position = new Vector3(aimPoint.transform.position.x, Mathf.Lerp(aimPoint.transform.position.y, ConstantValues.DefaultValues.GameCursor.InitialPosition.y, Time.unscaledDeltaTime), aimPoint.transform.position.z);
        }
        else if (GameState.currView == GameState.CameraView.SideView)
        {
            aimPoint.transform.position = (aimPoint.transform.position + new Vector3(0, mouseMoveY * aimPointSpeed, mouseMoveX * aimPointSpeed));
            aimPoint.transform.position = new Vector3(Mathf.Lerp(aimPoint.transform.position.x, ConstantValues.DefaultValues.GameCursor.InitialPosition.x, Time.unscaledDeltaTime), aimPoint.transform.position.y, aimPoint.transform.position.z);
        }
        else
        {
            aimPoint.transform.position = aimPoint.transform.position + new Vector3(mouseMoveX * aimPointSpeed, mouseMoveY * aimPointSpeed, 0);
            aimPoint.transform.position = new Vector3(aimPoint.transform.position.x, aimPoint.transform.position.y, Mathf.Lerp(aimPoint.transform.position.z, ConstantValues.DefaultValues.GameCursor.InitialPosition.z, Time.unscaledDeltaTime));
        }
    }

    void FixedUpdate()
    {
    }

    void Init()
    {
        aimPoint = GameObject.Instantiate(cursorObject);
        aimPoint.transform.position = ConstantValues.DefaultValues.GameCursor.InitialPosition;
        GameState.Player.aimPoint = aimPoint;
        Cursor.lockState = CursorLockMode.Locked;
    }

    public void ResetCrosshair() {
        aimPoint.transform.position = ConstantValues.DefaultValues.GameCursor.InitialPosition;
    }

    public void SetCrossHairRotation(bool camSwapFinished)
    {
        if (camSwapFinished)
        {
            switch (GameState.currView)
            {
                case GameState.CameraView.TopDown:
                    aimPoint.transform.rotation = Quaternion.Euler(ConstantValues.DefaultValues.GameCursor.TopDownView.Rotation);
                    break;
                case GameState.CameraView.SideView:
                    aimPoint.transform.rotation = Quaternion.Euler(ConstantValues.DefaultValues.GameCursor.SideView.Rotation);
                    break;
                case GameState.CameraView.Action:
                    aimPoint.transform.rotation = Quaternion.Euler(ConstantValues.DefaultValues.GameCursor.ActionView.Rotation);
                    break;
            }
        }
        else
        {
            aimPoint.transform.LookAt(GameState.mainCam.transform);
            resetting = true;
        }
    }
}
