﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlaneGame.ConstantVariables;
using PlaneGame.GameState;
using PlaneGame.UtilityFunctions;

public class Movement : MonoBehaviour {

    public float speed;
    public float rotateZMax;
    public float rotateXMax;
    public float rotateSpeed;
    public float rotateSmoothingSpeed;

    Quaternion rotation;
    float focusSpeed;
    float focusRotateZMax;
    float focusRotateXMax;

    float horizontalMovement = 0;
    float verticalMovement = 0;

    Vector2 limiter = new Vector2(1, 1);

    private Vector3 moveLimit;
    private BoxCollider unitCollider;
    // Use this for initialization
    void Start () {
        rotation = transform.rotation;
        focusSpeed = speed / 2;
        focusRotateZMax = rotateZMax / 2;
        focusRotateXMax = rotateXMax / 2;
        unitCollider = GetComponentInChildren<BoxCollider>();
    }
	
	// Update is called once per frame
	void Update () {
        horizontalMovement = Input.GetAxisRaw(ConstantValues.InputVariables.Axis.Horizontal);
        verticalMovement = Input.GetAxisRaw(ConstantValues.InputVariables.Axis.Vertical);
        unitCollider.transform.rotation = Quaternion.Euler(0,0,0);

        limiter = UtilityFunctions.ApplyPlaneMovementLimit(this.transform, horizontalMovement, verticalMovement);

        Quaternion newRotation = new Quaternion ();
        if (Input.GetAxisRaw(ConstantValues.InputVariables.Modifiers.FocusMove) > 0)
        {
            speed = focusSpeed;
            rotateZMax = focusRotateZMax;
            rotateXMax = focusRotateXMax;
        }
        else
        {
            speed = focusSpeed * 2;
            rotateZMax = focusRotateZMax * 2;
            rotateXMax = focusRotateXMax * 2;
        }

        float appliedSpeed = speed;
        if (horizontalMovement != 0 && verticalMovement != 0)
        {
            float pythagorean = Mathf.Sqrt((horizontalMovement * horizontalMovement) + (verticalMovement * verticalMovement));
            appliedSpeed = speed * (1 / pythagorean);
        }

        if (GameState.currView == GameState.CameraView.TopDown)
        {
            transform.position = new Vector3 (
                transform.position.x + appliedSpeed * Time.deltaTime * horizontalMovement * limiter.x,
                transform.position.y,
                transform.position.z + appliedSpeed * Time.deltaTime * verticalMovement * limiter.y
            );
            newRotation = Quaternion.Euler(new Vector3(Mathf.Clamp(rotateSpeed * verticalMovement, -rotateXMax, rotateXMax), 0, -Mathf.Clamp(rotateSpeed * horizontalMovement, -rotateZMax, rotateZMax)));
        }
        else if (GameState.currView == GameState.CameraView.Action)
        {
            transform.position = new Vector3(
                transform.position.x + appliedSpeed * Time.deltaTime * horizontalMovement * limiter.x,
                transform.position.y + appliedSpeed * Time.deltaTime * verticalMovement * limiter.y,
                transform.position.z
                );
        }
        else
        {
            // Side View
            transform.position = new Vector3(
                transform.position.x,
                transform.position.y + appliedSpeed * Time.deltaTime * verticalMovement * limiter.y,
                transform.position.z + appliedSpeed * Time.deltaTime * horizontalMovement * limiter.x
            );
            newRotation = Quaternion.Euler(new Vector3(Mathf.Clamp(rotateSpeed * horizontalMovement, -rotateXMax, rotateXMax), 0, 0));
        }

        transform.rotation = Quaternion.Slerp(transform.rotation, newRotation, Time.deltaTime * rotateSmoothingSpeed);
    }

    public void ResetPosition(GameState.CameraView currView, float speed)
    {
        float modifier = 1;
        Vector3 resetPosition = ConstantValues.DefaultValues.InitialSpawnSettings.SinglePlayer.Position;

        if (currView == GameState.CameraView.TopDown)
        {
            modifier = (transform.position.y > resetPosition.y) ? -1 : 1;

            transform.position = new Vector3(
                transform.position.x,
                Mathf.Clamp(transform.position.y + (modifier * speed * Time.deltaTime), Mathf.Min(resetPosition.y, transform.position.y), Mathf.Max(resetPosition.y, transform.position.y)),
                transform.position.z
            );

        }
        else if (currView == GameState.CameraView.SideView)
        {
            modifier = (transform.position.x > resetPosition.x) ? -1 : 1;

            transform.position = new Vector3(
                Mathf.Clamp(transform.position.x + (modifier * speed * Time.deltaTime), Mathf.Min(resetPosition.x, transform.position.x), Mathf.Max(resetPosition.x, transform.position.x)),
                transform.position.y,
                transform.position.z
            );
        }

        else
        {
            modifier = (transform.position.z > resetPosition.z) ? -1 : 1;

            transform.position = new Vector3(
                transform.position.x,
                transform.position.y,
                Mathf.Clamp(transform.position.z + (modifier * speed * Time.deltaTime), Mathf.Min(resetPosition.z, transform.position.z), Mathf.Max(resetPosition.z, transform.position.z))
            );
        }
    }
}
