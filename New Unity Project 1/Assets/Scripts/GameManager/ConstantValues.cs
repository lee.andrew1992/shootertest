using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PlaneGame.ConstantVariables
{
    public static class ConstantValues
    {
        public static class InputVariables
        {
            public static class Axis
            {
                public static string Horizontal = "Horizontal";
                public static string Vertical = "Vertical";
                public static string MouseX = "MouseX";
                public static string MouseY = "MouseY";
            }

            public static class Modifiers
            {
                public static string FocusMove = "FocusMove";
            }

            public static class Debug
            {
                public static string SpawnEnemies1 = "DebugSpawnEnemy1";
            }

            public static class ControlKeys
            {
                public static string Reload = "Reload";
                public static string WeaponSlot1 = "WeaponSlot1";
                public static string WeaponSlot2 = "WeaponSlot2";
                public static string WeaponSlot3 = "WeaponSlot3";
                public static string Fire = "Fire1";
                public static string ViewSwap = "ViewSwap";
            }
        }

        public static class DefaultValues
        {
            public static class InitialSpawnSettings
            {
                public static class SinglePlayer
                {
                    public static Vector3 Position = new Vector3 (0,1,-5);
                }
            }

            public static class GameCursor
            {
                public static Vector3 InitialPosition = new Vector3(0, 0, 1);

                public static class TopDownView
                {
                    public static Vector3 Rotation = new Vector3 (90,0,0);
                }

                public static class SideView
                {
                    public static Vector3 Rotation = new Vector3(180, 90, 90);
                }

                public static class ActionView
                {
                    public static Vector3 Rotation = new Vector3(180, 180, 90);
                }
            }

            public static class Camera
            {
                public static class ViewSwitch
                {
                    public static class TopDownView
                    {
                        public static float UnitRestrictionAxisY = 1;
                        public static Vector3 Position = new Vector3(0,16,0);
                    }

                    public static class ActionView
                    {
                        public static Vector3 Position = new Vector3(0,1,-8);
                    }

                    public static class SideView
                    {
                        public static float UnitRestrictionAxisX = 0;
                        public static Vector3 Position = new Vector3(16, 1, 0);
                    }

                    public static float SwapMaximumSpeed = 1f;
                    public static float SwapMinimumSpeed = 0.5f;
                }

                

                public static class ScreenCenter
                {
                    public static Vector3 Position = new Vector3(0,1,0);
                }
            }
        }

        public static class Enemies
        {
            public class Spawn
            {
                public enum FromLocation
                {
                    Front,
                    Right,
                    Left,
                    Back,
                    Bottom,
                    Top
                }

                public enum Shape
                {
                    Curve_n,
                    Curve_u,
                    Curve_j,
                    Curve_c,
                    Horizontal,
                    Vertical,
                    Wave
                }
                public enum ToLocation
                {
                    Center,
                    Left,
                    Right,
                    Back,
                    CenterWide
                }

                public static class OffScreenActivation
                {
                    public static Vector2 Right = new Vector2(20, 0);
                    public static Vector2 Left = new Vector2(-20, 0);
                    public static Vector2 Front = new Vector2(0, 20);
                    public static Vector2 Back = new Vector2(0, -20);
                }
            }

            public enum WeaponType
            {
                Automatic,
                Laser,
                Railgun
            }
        }

        public static class ObjectNames
        {
            public static class WeaponRelated
            {
                public static string Weapons = "Weapons";
                public static string SingleFire = "SingleFire";
                public static string AutoFire = "AutoFire";
            }

            public static string HitZones = "HitZones";
        }

        public static class Weapons
        {
            public static class WeaponSlots
            {
                public static int Slot1 = 0;
                public static int Slot2 = 1;
                public static int Slot3 = 2;
                public static int MaxSlots = 3;
            }

            public static class WeaponValues
            {
                public static float BulletLifeTime = 3f;
                public static float RecoilNormalize = 10f;

                public static class SingleFire
                {
                    public static class Pistol
                    {
                        public static string Moniker = "Pistol";
                        public static float Damage = 15;
                        public static float FireRate = 0.15f;
                        public static int ClipSize = 30;
                        public static float ReloadTime = 0.75f;
                        public static float BulletSpeed = 15;
                        public static float Recoil = 0.2f;
                        public static float MaxRecoil = 1;
                        public static float RecoilResetTime = 0.5f;
                    }
                }
                public static class AutoFire
                {
                    public static class MachineGun
                    {
                        public static string Moniker = "MachineGun";
                        public static float Damage = 12;
                        public static float FireRate = 0.05f;
                        public static int ClipSize = 80;
                        public static float ReloadTime = 1.5f;
                        public static float BulletSpeed = 20;
                        public static float Recoil = 0.06f;
                        public static float MaxRecoil = 1.5f;
                        public static float RecoilResetTime = 1f;
                    }
                }
            }
        }
    }
}