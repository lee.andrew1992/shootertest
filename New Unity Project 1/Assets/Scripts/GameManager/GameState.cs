﻿using System.Collections;
using System.Collections.Generic;
using PlaneGame.ConstantVariables;
using UnityEngine;
using System;

namespace PlaneGame.GameState
{
    public class GameState : MonoBehaviour
    {
        public enum CameraView
        {
            TopDown,
            SideView,
            Action
        }

        public static CameraView currView = CameraView.TopDown;
        public static GameState instance = null;
        public static Camera mainCam;
        public GameObject[] hitZones;

        public static List<EnemyGroup> createdEnemyGroups;
        public static int createdGroupsCount = 0;

        private static GameObject[] pv_hitZones;

        public GameObject unitGameObject;
        public float aimPointSpeed;
        
        public static GameObject currHitZone;

        public static class Player
        {
            public static GameObject unit;
            public static ViewSwitch viewSwitch;
            public static Inventory myInventory;
            public static List<Bullets> bulletList = new List<Bullets>();
            public static Crosshair myCrosshair;
            public static GameObject aimPoint;
        }

        void Awake() {
            pv_hitZones = new GameObject[hitZones.Length];
            if (mainCam == null)
            {
                mainCam = Camera.main;
            }

            if (instance == null)
            {
                instance = this;
            }
            else if (instance != this)
            {
                Destroy(gameObject);
            }

            DontDestroyOnLoad(gameObject);

            InitGame();
        }

        void InitGame()
        {
            Player.unit = Instantiate(unitGameObject, ConstantValues.DefaultValues.InitialSpawnSettings.SinglePlayer.Position, Quaternion.Euler(0,0,0));
            Player.myInventory = Player.unit.GetComponent<Inventory>();
            Player.myInventory.weaponSlots[0] = gameObject.transform.FindChild(ConstantValues.ObjectNames.WeaponRelated.Weapons).FindChild(ConstantValues.ObjectNames.WeaponRelated.SingleFire).GetComponent<SF_Pistol>();
            Player.myInventory.weaponSlots[1] = gameObject.transform.FindChild(ConstantValues.ObjectNames.WeaponRelated.Weapons).FindChild(ConstantValues.ObjectNames.WeaponRelated.AutoFire).GetComponent<AF_MachineGun>();
            for ( int index = 0; index < pv_hitZones.Length; index++ )
            {
                pv_hitZones[index] = Instantiate(hitZones[index], gameObject.transform.FindChild(ConstantValues.ObjectNames.HitZones));
            }

            Player.myCrosshair = Player.unit.GetComponent<Crosshair>();
            Player.myCrosshair.aimPointSpeed = aimPointSpeed;

            createdEnemyGroups = new List<EnemyGroup>();

            SetHitZone(currView);
        }

        public static void SetHitZone(CameraView currCamView)
        {
            switch (currCamView)
            {
                case CameraView.TopDown:
                    pv_hitZones[0].SetActive(true);
                    pv_hitZones[1].SetActive(false);
                    pv_hitZones[2].SetActive(false);
                    break;
                case CameraView.SideView:
                    pv_hitZones[0].SetActive(false);
                    pv_hitZones[1].SetActive(false);
                    pv_hitZones[2].SetActive(true);
                    break;
                case CameraView.Action:
                    pv_hitZones[0].SetActive(false);
                    pv_hitZones[1].SetActive(true);
                    pv_hitZones[2].SetActive(false);
                    break;
            }
        }
    }
}

