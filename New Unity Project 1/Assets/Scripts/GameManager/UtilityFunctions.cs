﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlaneGame.ConstantVariables;

namespace PlaneGame.UtilityFunctions
{
    public static class UtilityFunctions {

        public static float EulerAngleDifference(Vector3 eulerAngleA, Vector3 eulerAngleB)
        {
            Vector3 dupEulerAngleA = eulerAngleA;
            Vector3 dupEulerAngleB = eulerAngleB;

            dupEulerAngleA = new Vector3(EulerAngleToLowerQuad(dupEulerAngleA.x), EulerAngleToLowerQuad(dupEulerAngleA.y), EulerAngleToLowerQuad(dupEulerAngleA.z));
            dupEulerAngleB = new Vector3(EulerAngleToLowerQuad(dupEulerAngleB.x), EulerAngleToLowerQuad(dupEulerAngleB.y), EulerAngleToLowerQuad(dupEulerAngleB.z));


            return Vector3.Distance(dupEulerAngleA, dupEulerAngleB);
        }

        public static Vector2 ApplyPlaneMovementLimit(Transform limitedTransform, float xMove, float yMove)
        {
            Vector2 limiter = new Vector2();
            Vector2 limitVector = new Vector2();
            BoxCollider collider = limitedTransform.GetComponentInChildren<BoxCollider>();
            Vector3 colliderNormalizedCenter = Camera.main.WorldToViewportPoint(collider.bounds.center);
            Vector3 colliderSize = new Vector3(Mathf.Max(collider.bounds.max.x, collider.bounds.min.x) - Mathf.Min(collider.bounds.max.x, collider.bounds.min.x), Mathf.Max(collider.bounds.max.y, collider.bounds.min.y) - Mathf.Min(collider.bounds.max.y, collider.bounds.min.y), 0);


            if (colliderNormalizedCenter.x > 0.5)
            {
                limitVector.x = Camera.main.WorldToViewportPoint(collider.bounds.min + colliderSize).x;
            }
            else
            {
                limitVector.x = Camera.main.WorldToViewportPoint(collider.bounds.min - colliderSize).x;
            }
            if (colliderNormalizedCenter.y > 0.5)
            {
                limitVector.y = Camera.main.WorldToViewportPoint(collider.bounds.max - colliderSize).y;
            }
            else
            {
                limitVector.y = Camera.main.WorldToViewportPoint(collider.bounds.min + colliderSize).y;
            }

            //Debug.Log(limitVector.x + " " + xMove + " " +limitedTransform.name);


            if (limitVector.x >= 1f && xMove > 0 || limitVector.x <= 0f && xMove < 0)
            {
                limiter.x = 0;
            }
            else
            {
                limiter.x = 1;
            }

            if (limitVector.y >= 1f && yMove > 0 || limitVector.y <= 0 && yMove < 0)
            {
                limiter.y = 0;
            }
            else
            {
                limiter.y = 1;
            }

            return limiter;
        }

        public static Vector3 VectorAbs(Vector3 vector)
        {
            return new Vector3(Mathf.Abs(vector.x), Mathf.Abs(vector.y), Mathf.Abs(vector.z));
        }

        public static Vector2 VectorAbs(Vector2 vector)
        {
            return new Vector2(Mathf.Abs(vector.x), Mathf.Abs(vector.y));
        }

        public static float EulerAngleToLowerQuad(float angle)
        {
            if (angle > 180)
            {
                return 360 - angle;
            }
            else if (angle < -180)
            {
                return 360 + angle;
            }
            else
            {
                return angle;
            }
        }

        public static int GetMinDistanceElement(int [] array, int comparison)
        {
            int minElement = array[0];

            if (array.Length > 1)
            { 
                for (int index = 1; index < array.Length; index++)
                {
                    if (Mathf.Abs(array[index] - comparison) < Mathf.Abs(minElement - comparison))
                    {
                        minElement = array[index];
                    }
                }
            }

            return minElement;
        }
    }
}
