﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlaneGame.ConstantVariables;
using PlaneGame.GameState;
using PlaneGame.UtilityFunctions;
using System;

public class SpawnEnemies : MonoBehaviour
{

    public GameObject[] enemyTypes;
    public GameObject enemyGroupObject;
    GameObject allEnemies;

    private enum SpawnGeneralFormation
    {
        Horizontal,
        Vertical
    }

    // Use this for initialization
    void Start()
    {
        allEnemies = new GameObject();
        allEnemies.name = "AllEnemies";
    }

    // Update is called once per frame
    void Update()
    {
        //Debug Mode
        if (Input.GetButtonDown(ConstantValues.InputVariables.Debug.SpawnEnemies1))
        {
            Spawn(ConstantValues.Enemies.Spawn.FromLocation.Front, ConstantValues.Enemies.Spawn.Shape.Curve_n, ConstantValues.Enemies.WeaponType.Automatic, ConstantValues.Enemies.Spawn.ToLocation.Center, new Vector3(0, 0, 10), 6);
            Spawn(ConstantValues.Enemies.Spawn.FromLocation.Right, ConstantValues.Enemies.Spawn.Shape.Curve_j, ConstantValues.Enemies.WeaponType.Automatic, ConstantValues.Enemies.Spawn.ToLocation.Right, new Vector3(10, 0, 0), 6);
            Spawn(ConstantValues.Enemies.Spawn.FromLocation.Left, ConstantValues.Enemies.Spawn.Shape.Curve_c, ConstantValues.Enemies.WeaponType.Automatic, ConstantValues.Enemies.Spawn.ToLocation.Left, new Vector3(-10, 0, 0), 6);
            Spawn(ConstantValues.Enemies.Spawn.FromLocation.Back, ConstantValues.Enemies.Spawn.Shape.Curve_u, ConstantValues.Enemies.WeaponType.Automatic, ConstantValues.Enemies.Spawn.ToLocation.Center, new Vector3(0, 0, -10), 6);
            //Spawn(ConstantValues.Enemies.SpawnLocation.Front, ConstantValues.Enemies.SpawnShape.Horizontal, ConstantValues.Enemies.WeaponType.Automatic, ConstantValues.Enemies.DestSector.Center, new Vector3(0, 0, 0), 5);
            //Spawn(ConstantValues.Enemies.SpawnLocation.Front, ConstantValues.Enemies.SpawnShape.Vertical, ConstantValues.Enemies.WeaponType.Automatic, ConstantValues.Enemies.DestSector.Left, new Vector3(0, 0, 5), 3);
            //Spawn(ConstantValues.Enemies.SpawnLocation.Front, ConstantValues.Enemies.SpawnShape.Horizontal, ConstantValues.Enemies.WeaponType.Automatic, ConstantValues.Enemies.DestSector.Center, new Vector3(0, 0, 5), 4);
            //Spawn(ConstantValues.Enemies.SpawnLocation.Front, ConstantValues.Enemies.SpawnShape.Horizontal, ConstantValues.Enemies.WeaponType.Automatic, ConstantValues.Enemies.DestSector.Left, new Vector3(0, 0, 5), 4);
            //Spawn(ConstantValues.Enemies.SpawnLocation.Front, ConstantValues.Enemies.SpawnShape.Vertical, ConstantValues.Enemies.WeaponType.Automatic, ConstantValues.Enemies.DestSector.Left, new Vector3(0, 0, 5), 4);
            //Spawn(ConstantValues.Enemies.SpawnLocation.Front, ConstantValues.Enemies.SpawnShape.Vertical, ConstantValues.Enemies.WeaponType.Automatic, ConstantValues.Enemies.DestSector.Right, new Vector3(0, 0, 5), 4);
        }
    }

    public void Spawn(ConstantValues.Enemies.Spawn.FromLocation fromLoc, ConstantValues.Enemies.Spawn.Shape shape, ConstantValues.Enemies.WeaponType type, ConstantValues.Enemies.Spawn.ToLocation toLoc, Vector3 position, int count)
    {
        GameObject enemyGroup = Instantiate(enemyGroupObject, allEnemies.transform);
        GameState.createdGroupsCount++;
        enemyGroup.name = "EnemyGroup" + GameState.createdGroupsCount;
        GameObject[] enemyList = new GameObject[count];

        for (int index = 0; index < count; index++)
        {
            GameObject enemy = EnemyInstantiate(type, enemyGroup);
            enemyList[index] = enemy;
        }
        enemyGroup.GetComponent<EnemyGroup>().grouping = enemyList;

        switch (fromLoc)
        {
            case ConstantValues.Enemies.Spawn.FromLocation.Top:
                break;

            case ConstantValues.Enemies.Spawn.FromLocation.Right:
                foreach (GameObject enemy in enemyList)
                {
                    enemy.transform.position = new Vector3(ConstantValues.Enemies.Spawn.OffScreenActivation.Right.x, 1, ConstantValues.Enemies.Spawn.OffScreenActivation.Right.y);
                }
                break;

            case ConstantValues.Enemies.Spawn.FromLocation.Left:
                foreach (GameObject enemy in enemyList)
                {
                    enemy.transform.position = new Vector3(ConstantValues.Enemies.Spawn.OffScreenActivation.Left.x, 1, ConstantValues.Enemies.Spawn.OffScreenActivation.Left.y);
                }
                break;

            case ConstantValues.Enemies.Spawn.FromLocation.Front:
                foreach (GameObject enemy in enemyList)
                {
                    enemy.transform.position = new Vector3(ConstantValues.Enemies.Spawn.OffScreenActivation.Front.x, 1, ConstantValues.Enemies.Spawn.OffScreenActivation.Front.y);
                }
                break;

            case ConstantValues.Enemies.Spawn.FromLocation.Back:
                foreach (GameObject enemy in enemyList)
                {
                    enemy.transform.position = new Vector3(ConstantValues.Enemies.Spawn.OffScreenActivation.Back.x, 1, ConstantValues.Enemies.Spawn.OffScreenActivation.Back.y);
                }
                break;

            case ConstantValues.Enemies.Spawn.FromLocation.Bottom:
                break;
        }

        ApplySpacing(enemyGroup.GetComponent<EnemyGroup>(), toLoc, shape);
        StartCoroutine(ApplyShape(shape, fromLoc, toLoc, enemyGroup.GetComponent<EnemyGroup>(), position));
    }

    private IEnumerator ApplyShape(ConstantValues.Enemies.Spawn.Shape shape, ConstantValues.Enemies.Spawn.FromLocation fromLoc, ConstantValues.Enemies.Spawn.ToLocation toLoc, EnemyGroup enemyGroup, Vector3 pivotPosition)
    {
        int groupCount = enemyGroup.grouping.Length;
        Vector3 destPosition = new Vector3();
        float closerHeading;
        for (int index = 0; index < groupCount; index++)
        {
            closerHeading = UtilityFunctions.GetMinDistanceElement(enemyGroup.groupingHeadIndices, index);
            switch (shape)
            {
                case ConstantValues.Enemies.Spawn.Shape.Curve_n:
                case ConstantValues.Enemies.Spawn.Shape.Curve_u:
                    float spacingZ = 0;
                    if (Array.IndexOf(enemyGroup.groupingHeadIndices, index) == -1)
                    {
                        float formulaDenominator = (groupCount / 2) - Mathf.Abs(index - closerHeading) + 1;
                        spacingZ = enemyGroup.spacingZ / (formulaDenominator * formulaDenominator);
                        if (shape == ConstantValues.Enemies.Spawn.Shape.Curve_u)
                        {
                            spacingZ = -spacingZ;
                        }
                    }

                    if (groupCount % 2 == 1)
                    {
                        destPosition = new Vector3(pivotPosition.x + ((closerHeading - index) * -enemyGroup.spacingX), pivotPosition.y, pivotPosition.z + spacingZ);
                    }
                    else
                    {
                        if (index <= enemyGroup.groupingHeadIndices[0])
                        {
                            destPosition = new Vector3(pivotPosition.x - enemyGroup.spacingX * (closerHeading - index + 0.5f), pivotPosition.y, pivotPosition.z + spacingZ);
                        }
                        else if (enemyGroup.groupingHeadIndices[1] == index || index >= enemyGroup.groupingHeadIndices[0])
                        {
                            //destPosition = new Vector3(pivotPosition.x - enemyGroup.spacingX * (closerHeading - index - 0.5f), pivotPosition.y, pivotPosition.z + ((closerHeading - index - 0.5f) * (-enemyGroup.spacingZ + spacingZ)));
                            destPosition = new Vector3(pivotPosition.x - enemyGroup.spacingX * (closerHeading - index - 0.5f), pivotPosition.y, pivotPosition.z + spacingZ);
                        }
                    }
                    enemyGroup.grouping[index].GetComponent<EnemyLogic>().Init(fromLoc, destPosition, 15);
                    break;

                case ConstantValues.Enemies.Spawn.Shape.Curve_c:
                case ConstantValues.Enemies.Spawn.Shape.Curve_j:
                    float spacingX = 0;
                    
                    if (Array.IndexOf(enemyGroup.groupingHeadIndices, index) == -1)
                    {
                        float formulaDenominator = (groupCount / 2) - Mathf.Abs(index - closerHeading) + 1;
                        spacingX = enemyGroup.spacingZ / (formulaDenominator * formulaDenominator);
                        if (shape == ConstantValues.Enemies.Spawn.Shape.Curve_c)
                        {
                            spacingX = -spacingX;
                        }
                    }

                    if (groupCount % 2 == 1)
                    {
                        destPosition = new Vector3(pivotPosition.x + spacingX, pivotPosition.y, pivotPosition.z + ((closerHeading - index) * -enemyGroup.spacingZ));
                    }
                    else
                    {
                        if (index <= enemyGroup.groupingHeadIndices[0])
                        {
                            destPosition = new Vector3(pivotPosition.x + spacingX * (closerHeading - index + 0.5f), pivotPosition.y, pivotPosition.z + ((closerHeading - index + 0.5f) * -enemyGroup.spacingZ));
                        }
                        else if (enemyGroup.groupingHeadIndices[1] == index || index >= enemyGroup.groupingHeadIndices[0])
                        {
                            destPosition = new Vector3(pivotPosition.x - spacingX * (closerHeading - index - 0.5f), pivotPosition.y, pivotPosition.z + ((closerHeading - index - 0.5f) * -enemyGroup.spacingZ));
                        }
                    }
                    enemyGroup.grouping[index].GetComponent<EnemyLogic>().Init(fromLoc, destPosition, 15);
                    break;

                case ConstantValues.Enemies.Spawn.Shape.Vertical:   
                    if (groupCount % 2 == 1)
                    {
                        destPosition = new Vector3(pivotPosition.x, pivotPosition.y, pivotPosition.z + ((closerHeading - index) * -enemyGroup.spacingZ));
                    }

                    else
                    {
                        if (index <= enemyGroup.groupingHeadIndices[0])
                        {
                            destPosition = new Vector3(pivotPosition.x, pivotPosition.y, pivotPosition.z + ((closerHeading - index + 0.5f) * -enemyGroup.spacingZ));
                        }
                        else if (enemyGroup.groupingHeadIndices[1] == index || index >= enemyGroup.groupingHeadIndices[0])
                        {
                            destPosition = new Vector3(pivotPosition.x, pivotPosition.y, pivotPosition.z + ((closerHeading - index - 0.5f) * -enemyGroup.spacingZ));
                        }
                    }
                    enemyGroup.grouping[index].GetComponent<EnemyLogic>().Init(fromLoc, destPosition, 15);
                    break;

                case ConstantValues.Enemies.Spawn.Shape.Horizontal:
                    if (groupCount % 2 == 1)
                    {
                        destPosition = new Vector3(pivotPosition.x + ((closerHeading - index) * -enemyGroup.spacingX), pivotPosition.y, pivotPosition.z);
                    }
                    else
                    {
                        if (index <= enemyGroup.groupingHeadIndices[0])
                        {
                            destPosition = new Vector3(pivotPosition.x + ((closerHeading - index + 0.5f) * -enemyGroup.spacingX), pivotPosition.y, pivotPosition.z);
                        }
                        else if (enemyGroup.groupingHeadIndices[1] == index || index >= enemyGroup.groupingHeadIndices[0])
                        {
                            destPosition = new Vector3(pivotPosition.x + ((closerHeading - index - 0.5f) * -enemyGroup.spacingX), pivotPosition.y, pivotPosition.z);
                        }
                    }
                    enemyGroup.grouping[index].GetComponent<EnemyLogic>().Init(fromLoc, destPosition, 15);
                    break;

                case ConstantValues.Enemies.Spawn.Shape.Wave:
                    break;
            }

            yield return null;
        }
    }

    private GameObject EnemyInstantiate(ConstantValues.Enemies.WeaponType type, GameObject group)
    {
        GameObject enemy = null;
        switch (type)
        {
            case ConstantValues.Enemies.WeaponType.Automatic:
                enemy = Instantiate(enemyTypes[0], group.transform);
                break;

            case ConstantValues.Enemies.WeaponType.Laser:
                enemy = Instantiate(enemyTypes[1], group.transform);
                break;

            case ConstantValues.Enemies.WeaponType.Railgun:
                enemy = Instantiate(enemyTypes[2], group.transform);
                break;
        }
        
        return enemy;
    }

    private void ApplySpacing(EnemyGroup enemyGroup, ConstantValues.Enemies.Spawn.ToLocation toLoc, ConstantValues.Enemies.Spawn.Shape shape)
    {
        GameObject[] enemyArray = enemyGroup.grouping;
        int count = enemyArray.Length;

        float centerPoint = 0;
        float rightMax = 0;
        float leftMax = 0;
        float topMax = 0;
        float bottomMax = 0;
        float individualSpacing = 0;

        switch (toLoc)
        {
            case ConstantValues.Enemies.Spawn.ToLocation.Center:
                centerPoint = Camera.main.ViewportToWorldPoint(new Vector3(0.5f, 1, Camera.main.transform.position.y)).x;
                rightMax = Camera.main.ViewportToWorldPoint(new Vector3(0.75f, 1, Camera.main.transform.position.y)).x;
                leftMax = Camera.main.ViewportToWorldPoint(new Vector3(0.25f, 1, Camera.main.transform.position.y)).x;
                topMax = Camera.main.ViewportToWorldPoint(new Vector3(1, 0f, Camera.main.transform.position.y)).z;
                bottomMax = Camera.main.ViewportToWorldPoint(new Vector3(1f, 1f, Camera.main.transform.position.y)).z;
                break;

            case ConstantValues.Enemies.Spawn.ToLocation.CenterWide:
                centerPoint = Camera.main.ViewportToWorldPoint(new Vector3(0.5f, 1, Camera.main.transform.position.y)).x;
                rightMax = Camera.main.ViewportToWorldPoint(new Vector3(1, 1, Camera.main.transform.position.y)).x;
                leftMax = Camera.main.ViewportToWorldPoint(new Vector3(0, 1, Camera.main.transform.position.y)).x;
                topMax = Camera.main.ViewportToWorldPoint(new Vector3(1, 0f, Camera.main.transform.position.y)).z;
                bottomMax = Camera.main.ViewportToWorldPoint(new Vector3(1f, 1f, Camera.main.transform.position.y)).z;
                break;

            case ConstantValues.Enemies.Spawn.ToLocation.Left:
                centerPoint = Camera.main.ViewportToWorldPoint(new Vector3(0.2f, 1, Camera.main.transform.position.y)).x;
                rightMax = Camera.main.ViewportToWorldPoint(new Vector3(0.4f, 1, Camera.main.transform.position.y)).x;
                leftMax = Camera.main.ViewportToWorldPoint(new Vector3(0, 1, Camera.main.transform.position.y)).x;
                topMax = Camera.main.ViewportToWorldPoint(new Vector3(1, 0f, Camera.main.transform.position.y)).z;
                bottomMax = Camera.main.ViewportToWorldPoint(new Vector3(1, 1f, Camera.main.transform.position.y)).z;
                break;

            case ConstantValues.Enemies.Spawn.ToLocation.Right:
                centerPoint = Camera.main.ViewportToWorldPoint(new Vector3(0.8f, 1, Camera.main.transform.position.y)).x;
                rightMax = Camera.main.ViewportToWorldPoint(new Vector3(1f, 1, Camera.main.transform.position.y)).x;
                leftMax = Camera.main.ViewportToWorldPoint(new Vector3(0.6f, 1, Camera.main.transform.position.y)).x;
                topMax = Camera.main.ViewportToWorldPoint(new Vector3(1, 0f, Camera.main.transform.position.y)).z;
                bottomMax = Camera.main.ViewportToWorldPoint(new Vector3(1, 1f, Camera.main.transform.position.y)).z;
                break;
        }

        if (count % 2 == 1)
        {
            // odd
            if (shape == ConstantValues.Enemies.Spawn.Shape.Curve_n || shape == ConstantValues.Enemies.Spawn.Shape.Curve_u || shape == ConstantValues.Enemies.Spawn.Shape.Horizontal)
            {
                individualSpacing = (rightMax - leftMax) / (count + 2);
                enemyGroup.spacingX = individualSpacing;
                enemyGroup.spacingY = 0;
                enemyGroup.spacingZ = 0;

                if (shape == ConstantValues.Enemies.Spawn.Shape.Curve_u || shape == ConstantValues.Enemies.Spawn.Shape.Curve_n)
                {
                    enemyGroup.spacingZ = (topMax - bottomMax) / (count + 2);
                }

                SetSpacingOdd(enemyGroup, individualSpacing, centerPoint, SpawnGeneralFormation.Horizontal);
            }

            if (shape == ConstantValues.Enemies.Spawn.Shape.Curve_j || shape == ConstantValues.Enemies.Spawn.Shape.Curve_c || shape == ConstantValues.Enemies.Spawn.Shape.Vertical)
            {
                individualSpacing = (topMax - bottomMax) / (count + 2);
                enemyGroup.spacingY = 0;
                enemyGroup.spacingZ = individualSpacing;
                enemyGroup.spacingX = 0;

                if (shape == ConstantValues.Enemies.Spawn.Shape.Curve_j || shape == ConstantValues.Enemies.Spawn.Shape.Curve_c)
                {
                    enemyGroup.spacingX = (rightMax - leftMax) / (count + 2);
                }
                SetSpacingOdd(enemyGroup, individualSpacing, centerPoint, SpawnGeneralFormation.Vertical);
            }
        }
        else
        {
            // even
            if (shape == ConstantValues.Enemies.Spawn.Shape.Curve_n || shape == ConstantValues.Enemies.Spawn.Shape.Curve_u || shape == ConstantValues.Enemies.Spawn.Shape.Horizontal)
            {
                individualSpacing = (rightMax - leftMax) / (count + 2);
                enemyGroup.spacingX = individualSpacing;
                enemyGroup.spacingY = 0;
                enemyGroup.spacingZ = 0;
                if (shape == ConstantValues.Enemies.Spawn.Shape.Curve_n || shape == ConstantValues.Enemies.Spawn.Shape.Curve_u)
                {
                    enemyGroup.spacingZ = (topMax - bottomMax) / (count + 2);
                }
                SetSpacingEven(enemyGroup, individualSpacing, centerPoint, SpawnGeneralFormation.Horizontal);
            }

            else if (shape == ConstantValues.Enemies.Spawn.Shape.Curve_c || shape == ConstantValues.Enemies.Spawn.Shape.Curve_j || shape == ConstantValues.Enemies.Spawn.Shape.Vertical)
            {
                individualSpacing = (topMax - bottomMax) / (count + 2);
                enemyGroup.spacingX = 0;
                enemyGroup.spacingY = 0;
                enemyGroup.spacingZ = individualSpacing;
                if (shape == ConstantValues.Enemies.Spawn.Shape.Curve_c || shape == ConstantValues.Enemies.Spawn.Shape.Curve_j)
                {
                    enemyGroup.spacingX = (rightMax - leftMax) / (count + 2);
                }
                SetSpacingEven(enemyGroup, individualSpacing, centerPoint, SpawnGeneralFormation.Vertical);
            }
        }
    }

    private void SetSpacingOdd(EnemyGroup group, float individualSpacing, float centerPoint, SpawnGeneralFormation generalForm)
    {
        GameObject[] enemyGroup = group.grouping;
        int midIndex = (int)(group.grouping.Length / 2);
        int lowerIndex = midIndex;
        int higherIndex = midIndex;
        int loopIndex = 0;

        group.groupingHeadIndices = new int[] { lowerIndex };

        switch (generalForm)
        {
            case SpawnGeneralFormation.Horizontal:
                while (loopIndex <= midIndex)
                {
                    if (lowerIndex != higherIndex)
                    {
                        enemyGroup[higherIndex].transform.position = new Vector3(enemyGroup[higherIndex].transform.position.x + centerPoint + (loopIndex * individualSpacing), enemyGroup[higherIndex].transform.position.y, enemyGroup[higherIndex].transform.position.z);
                        enemyGroup[lowerIndex].transform.position = new Vector3(enemyGroup[lowerIndex].transform.position.x + centerPoint - (loopIndex * individualSpacing), enemyGroup[lowerIndex].transform.position.y, enemyGroup[lowerIndex].transform.position.z);
                    }
                    else
                    {
                        enemyGroup[lowerIndex].transform.position = new Vector3(enemyGroup[lowerIndex].transform.position.x + centerPoint, enemyGroup[lowerIndex].transform.position.y, enemyGroup[lowerIndex].transform.position.z);
                    }

                    loopIndex++;
                    lowerIndex--;
                    higherIndex++;
                }
                break;

            case SpawnGeneralFormation.Vertical:
                while (loopIndex <= midIndex)
                {
                    if (lowerIndex != higherIndex)
                    {
                        enemyGroup[higherIndex].transform.position = new Vector3(enemyGroup[higherIndex].transform.position.x + centerPoint, enemyGroup[higherIndex].transform.position.y, enemyGroup[higherIndex].transform.position.z + (loopIndex * individualSpacing));
                        enemyGroup[lowerIndex].transform.position = new Vector3(enemyGroup[lowerIndex].transform.position.x + centerPoint, enemyGroup[lowerIndex].transform.position.y, enemyGroup[lowerIndex].transform.position.z - (loopIndex * individualSpacing));
                    }
                    else
                    {
                        enemyGroup[lowerIndex].transform.position = new Vector3(enemyGroup[lowerIndex].transform.position.x + centerPoint, enemyGroup[lowerIndex].transform.position.y, enemyGroup[lowerIndex].transform.position.z);
                    }

                    loopIndex++;
                    lowerIndex--;
                    higherIndex++;
                }
                break;
        }
    }

    private void SetSpacingEven(EnemyGroup group, float individualSpacing, float centerPoint, SpawnGeneralFormation generalForm)
    {
        GameObject[] enemyGroup = group.grouping;
        int higherIndex = enemyGroup.Length / 2;
        int lowerIndex = higherIndex - 1;
        float loopIndex = 0;

        group.groupingHeadIndices = new int[] { lowerIndex, higherIndex };

        switch (generalForm)
        {
            case SpawnGeneralFormation.Horizontal:
                while (higherIndex <= enemyGroup.Length && lowerIndex >= 0)
                {
                    enemyGroup[higherIndex].transform.position = new Vector3(enemyGroup[higherIndex].transform.position.x + centerPoint + ((loopIndex + 0.5f) * individualSpacing), enemyGroup[higherIndex].transform.position.y, enemyGroup[higherIndex].transform.position.z);
                    enemyGroup[lowerIndex].transform.position = new Vector3(enemyGroup[lowerIndex].transform.position.x + centerPoint - ((loopIndex + 0.5f) * individualSpacing), enemyGroup[lowerIndex].transform.position.y, enemyGroup[lowerIndex].transform.position.z);

                    loopIndex++;
                    lowerIndex--;
                    higherIndex++;
                }
                break;

            case SpawnGeneralFormation.Vertical:
                while (higherIndex <= enemyGroup.Length && lowerIndex >= 0)
                {
                    enemyGroup[higherIndex].transform.position = new Vector3(enemyGroup[higherIndex].transform.position.x + centerPoint, enemyGroup[higherIndex].transform.position.y, enemyGroup[higherIndex].transform.position.z + ((loopIndex + 0.5f) * individualSpacing));
                    enemyGroup[lowerIndex].transform.position = new Vector3(enemyGroup[lowerIndex].transform.position.x  + centerPoint, enemyGroup[lowerIndex].transform.position.y, enemyGroup[lowerIndex].transform.position.z - ((loopIndex + 0.5f) * individualSpacing));

                    loopIndex++;
                    lowerIndex--;
                    higherIndex++;
                }
                break;
        }
    }
}
