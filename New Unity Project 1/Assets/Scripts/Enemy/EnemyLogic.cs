﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlaneGame.ConstantVariables;
using PlaneGame.GameState;

public class EnemyLogic : MonoBehaviour {

    private PositionState currPosState;
    private PositionState originalPosState;
    private Vector3 initPosition;
    public float initSpeed;

    public float maxHealth;
    public float currHealth;

    public enum PositionState
    {
        None,
        Spawn,
        SideView,
        TopDownView,
        ActionView
    }

	// Use this for initialization
	void Start () {
        currPosState = PositionState.Spawn;
        switch (GameState.currView)
        {
            case GameState.CameraView.Action:
                originalPosState = PositionState.ActionView;
                break;
            case GameState.CameraView.SideView:
                originalPosState = PositionState.SideView;
                break;
            case GameState.CameraView.TopDown:
                originalPosState = PositionState.TopDownView;
                break;
        }
	}
	
	// Update is called once per frame
	void Update () {
        switch (currPosState)
        {
            case PositionState.Spawn:
            case PositionState.ActionView:
                this.transform.position = Vector3.MoveTowards(this.transform.position, initPosition, Time.deltaTime * initSpeed);
                break;

            case PositionState.SideView:
                this.transform.position = Vector3.MoveTowards(this.transform.position, new Vector3(0, initPosition.y, initPosition.z), Time.deltaTime * initSpeed);
                break;

            case PositionState.TopDownView:
                this.transform.position = Vector3.MoveTowards(this.transform.position, new Vector3(initPosition.x, 1, initPosition.z), Time.deltaTime * initSpeed);
                break;
        }
	}

    public void Init(ConstantValues.Enemies.Spawn.FromLocation fromLoc, Vector3 initPosition, float initSpeed)
    {
        currPosState = PositionState.Spawn;
        SetInitSettings(initPosition, initSpeed);
    }

    private void SetInitSettings(Vector3 initPosition, float initSpeed)
    {
        this.initPosition = initPosition;
        this.initSpeed = initSpeed;
    }

    public void MoveToViewPosition(GameState.CameraView currView)
    {
        switch (currView)
        {
            case GameState.CameraView.Action:
                currPosState = PositionState.ActionView;
                break;
            case GameState.CameraView.SideView:
                currPosState = PositionState.SideView;
                break;
            case GameState.CameraView.TopDown:
                currPosState = PositionState.TopDownView;
                break;
        }
    }
}
