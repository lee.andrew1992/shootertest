﻿using System.Collections;
using System.Collections.Generic;
using PlaneGame.GameState;
using UnityEngine;

public class EnemyGroup : MonoBehaviour {

    public GameObject[] grouping;
    public int [] groupingHeadIndices;
    public float spacingX;
    public float spacingY;
    public float spacingZ;
    public GameState.CameraView spawnedView;

	// Use this for initialization
	void Start () {
        GameState.createdEnemyGroups.Add(this);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetEnemyViewPosition(GameState.CameraView newView)
    {
        foreach (GameObject enemy in grouping)
        {
            enemy.GetComponent<EnemyLogic>().MoveToViewPosition(newView);
        }
    }
}
